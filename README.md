# Netlink Library

`netlink-go` is a netlink API for golang. It is designed to be easy to use and abstract away some of the lifecycle bulk and boilerplate.

**THIS PROJECT IS NOT YET USABLE AND SHOULD BE AVOIDED FOR ALL CRITICAL/PRODUCTION SYSTEMS**

This is, for now, a mostly learning exercise. A stable version is being worked towards, that will support all of the RTNETLINK features.

# Supported Features and Modules

## RTNETLINK

[RTNETLINK](https://man7.org/linux/man-pages/man7/rtnetlink.7.html) refers to the linux routing socket. It allows for reading and altering the kernel's routing tables. 

Currently, only notifications for new and deleted routes are supported

# Examples

## Simple Alert Notify
```go
c, err := netlink.New()
if err != nil {
    //handle err
}

log.Println("Register alert handler")
notifyChan := make(chan *netlink.Message, 100)
c.Notify(notifyChan)

for msg := range notifyChan {
    //handle message
}
```

## Simple Alert Callback
```go
c, err := netlink.New()
if err != nil {
    //handle err
}

log.Println("Register alert handler")
notifyChan := make(chan *netlink.Message, 100)
c.Handle(func(m *netlink.Message){
    //handle m
})
```
