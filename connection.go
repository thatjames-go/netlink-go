package netlink

import (
	"bytes"
	"encoding/binary"
	"errors"
	"os"
	"syscall"

	"golang.org/x/sys/unix"
)

//MessageHandler is a function type that defines a basic callback type
type MessageHandler func(*Message)

//Connection is a wrapper to the underlying linux socket that communicates with the kernel module.
//Despite its name, the underyling communication is not a long lived. Binds to the socket only happen when the Connection is ready to poll,
//and they are closed once they are read (or a timeout is hit)
type Connection struct {
	opts        *ConnectionOptions
	messageChan chan *Message
	notifyChan  chan *Message
	runLock     chan interface{}
	exitChan    chan interface{}
	handler     MessageHandler
}

//ConnectionOptions defines the flag passed at bind time to the kernel socket. It contains the necessary parameters to define which
//messages and sub-modules should be available to this socket
type ConnectionOptions struct {
	Family uint16
	Groups uint32
}

//NewWithOpts returns a new Connection instance that will bind to the kernel socket using the specified parameter flags
func NewWithOpts(opts *ConnectionOptions) (*Connection, error) {
	c := &Connection{
		messageChan: make(chan *Message, 100),
		runLock:     make(chan interface{}),
		exitChan:    make(chan interface{}),
		opts:        opts,
	}

	go c.read()
	go c.run()
	return c, nil
}

//Net returns a new Connection instance that will bind to the kernel socket using the AF_NETLINK and RTMGRP_IPV4_ROUTE flags
func New() (*Connection, error) {
	return NewWithOpts(&ConnectionOptions{
		Family: unix.AF_NETLINK,
		Groups: unix.RTMGRP_IPV4_ROUTE,
	})
}

//Close instructs the running routines to terminate
func (z *Connection) Close() error {
	close(z.messageChan)
	close(z.exitChan)
	return nil
}

//SendMessage marshals a payload and delivers it to the kernel socket.
//Currently not implemented
func (z *Connection) SendMessage(m *Message) error {
	return errors.New("not implemented yet")
}

//Notify registers a provided chan *Message that the connection will write all incoming requests to. Notify takes precedence over Handle
func (z *Connection) Notify(c chan *Message) {
	z.notifyChan = c
}

//Handle registers a function callback that all incoming messages will be passed to. Notify takes precedence over handle, and this method will not be called
// if a notify channel has been registered (and will also stop being called after a notify channel has been registered)
func (z *Connection) Handle(h MessageHandler) {
	z.handler = h
}

func (z *Connection) run() {
	for {
		select {
		case msg := <-z.messageChan:
			if z.notifyChan != nil {
				z.notifyChan <- msg
			} else if z.handler != nil {
				z.handler(msg)
			}

		case z.runLock <- nil:

		case <-z.exitChan:
			return

		}
	}
}

func (z *Connection) read() {
	for range z.runLock {
		msg, err := readPacket(z.opts)
		if err != nil {
			continue
		}
		z.messageChan <- msg
	}
}

func readPacket(opts *ConnectionOptions) (msg *Message, err error) {
	fd, err := unix.Socket(int(opts.Family), unix.SOCK_RAW, unix.NETLINK_ROUTE)
	if err != nil {
		return nil, err
	}

	timeoutVal := syscall.NsecToTimeval(1000 * 1000 * 5000) //5s timeout
	if err := syscall.SetsockoptTimeval(fd, syscall.SOL_SOCKET, syscall.SO_RCVTIMEO, &timeoutVal); err != nil {
		return nil, err
	}

	if err := unix.Bind(fd, &unix.SockaddrNetlink{
		Family: opts.Family,
		Groups: opts.Groups,
	}); err != nil {
		return nil, err
	}

	defer syscall.Close(fd)

	var (
		n int
		b []byte = make([]byte, os.Getpagesize())
	)
	for {
		if n, _, err = unix.Recvfrom(fd, b, unix.MSG_PEEK); err != nil {
			return
		}

		if n < len(b) {
			break
		}

		b = make([]byte, len(b)*2)
	}

	if n, _, err = unix.Recvfrom(fd, b, 0); err != nil {
		return
	}

	var hdr Header
	buff := bytes.NewBuffer(b[:n])
	if err = binary.Read(buff, binary.LittleEndian, &hdr); err != nil {
		return
	}

	msg = new(Message)
	msg.Header = hdr
	msg.Data = buff.Bytes()
	return
}
