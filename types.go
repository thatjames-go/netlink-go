//netlink is a basic wrapper for the linux netlink kernel module, designed to be interacted with from userspace.
package netlink

import (
	"bytes"
	"encoding/binary"
)

//Header is a basic type wrapper for netlink header messages
type Header struct {
	Length uint32
	Type   uint16
	Flags  uint16
	Pid    uint32
}

//Message is a basic type wrapper that extends a header with the corresponding data body (if present)
type Message struct {
	Header
	Data []byte
}

//DecodeMessage unmarshals a byte slice into a Message pointer, returning an error if
//the raw bytes cannot be unmarshalled correctly
func DecodeMessge(b []byte) (*Message, error) {
	var hdr Header
	buff := bytes.NewBuffer(b)
	if err := binary.Read(buff, binary.LittleEndian, &hdr); err != nil {
		return nil, err
	}

	return &Message{
		Header: hdr,
		Data:   buff.Bytes(),
	}, nil
}
